//
// Created by S. Jansen on 8/14/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_EFFECTSCREEN_H
#define LCDMOVINGHEADCONTROLLER_EFFECTSCREEN_H

#include <Arduino.h>
#include "Screen.h"
#include "Button.h"
#include "lib/movingheadcommunicationprotocol/MovingHeadCommunicationProtocol.h"

class EffectScreen : public Screen {
private:
    enum Effect {
        OFF,
        SOLID,
        STROBE,
        STARS,
        SMILEY,
        RAINBOW
    };
    char effectNames[6][8];

    Button backButton;
    Button effectButtons[6];
    uint16_t speedSliderPositionX = 270;
    uint16_t speedSliderPositionY = 40;
    uint16_t speedSliderWidth = 20;
    uint16_t speedSliderHeight = 150;

    void initializeBackButton();
    void initializeEffectButtons();
    void displayBackButton();
    void displayEffectButtons();
    void displaySpeedSlider();
    void displayEffectSend(uint8_t effect);
    void displaySpeedSliderValue(uint8_t speed);
    void getNextButtonPosition(int *currentX, int *currentY,  Button effectButton, int buttonMargins);

public:
    EffectScreen(UTFT *screen);
    void display();
    uint8_t isBackButtonTouched(uint16_t x, uint16_t y);
    uint8_t isEffectButtonTouched(uint16_t x, uint16_t y, uint8_t *resultEffect);
    uint8_t isSpeedSliderTouched(uint16_t x, uint16_t y, uint8_t *resultSpeed);
    void sendEffect(uint8_t effect);
    void sendSpeed(uint8_t speed);
};


#endif //LCDMOVINGHEADCONTROLLER_EFFECTSCREEN_H
