# LCDMovingHeadController

_A touchscreen Arduino powered controller for the Arduino [MovingHead](https://bitbucket.org/sajansen/movinghead/)_

### Setup

#### Hardware

##### Requirements

* Arduino Mega
* LCD shield
* LCD touchscreen
* \>=3 pole plug for the serial connection to the MovingHead

##### Installation

* Plug the LCD touchscreen into the shield on top of the Arduino Mega.
* Connect two of the three terminals of the serial connection plug to the Serial1 pins of the Arduino Mega (see [this reference](https://www.arduino.cc/reference/en/language/functions/communication/serial/)). 
* You're good to go!


#### Software

**Configuration**

You might (must!) want to configure the `UTFT screen()` and `URTouch touch()` variables in [LCDMovingHeadController.ino](LCDMovingHeadController.ino) to match your screen.

**Libraries**

It may be that you need to import the [MovingHeadCommunicationProtocol](https://bitbucket.org/sajansen/movingheadcommunicationprotocol/) as a Library into the Arduino IDE. Also, the [URTouch](http://www.rinkydinkelectronics.com/library.php?id=92) and [UTFT](http://www.rinkydinkelectronics.com/library.php?id=51) libraries are required. You can download the latest version and otherwise I've still included them in the [lib/](lib/) folder.

The MovingHeadCommunicationProtocol is a git submodule, so you need to check that one out by running:
```bash
git submodule update --init --recursive
```

Upload the [LCDMovingHeadController.ino](LCDMovingHeadController.ino) sketch to your Mega and done!


##### Custom screens

You can add extra screens by creating a new child class of [Screen](Screen.h). Initialize this new screen in [LCDMovingHeadController.ino](LCDMovingHeadController.ino). Make sure you also add it's event handlers to the handleTouch() function in [LCDMovingHeadController.ino](LCDMovingHeadController.ino). That's it! 

### Future

I made a start with recording movements in order to repeatedly playback these movements to the MovingHead. Due to the lack of RAM, the max recording datapoints is very limited. 

Also, RGB color sliders would be simple but awesome to add.
