//
// Created by S. Jansen on 8/10/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_COLORBUTTON_H
#define LCDMOVINGHEADCONTROLLER_COLORBUTTON_H

#include <Arduino.h>
#include <UTFT.h>
#include "Button.h"

class ColorButton: public Button {
public:
    ColorButton() : Button(){};

private:
    // Hide other functions and variables
    using Button::setText;
    using Button::setBorderColor;
    using Button::setTextColor;

    using Button::text;
    using Button::borderColorR;
    using Button::borderColorG;
    using Button::borderColorB;
    using Button::textColorR;
    using Button::textColorG;
    using Button::textColorB;
};

#endif //LCDMOVINGHEADCONTROLLER_COLORBUTTON_H
