//
// Created by S. Jansen on 8/10/19.
//

#include "ColorScreen.h"

ColorScreen::ColorScreen(UTFT *screen) : Screen(screen) {
    initializeBackButton();
    initializeColorButtons();
}

void ColorScreen::initializeBackButton() {
    Screen::screen->setFont(SmallFont);

    char text[] = "Back";
    int textLength = sizeof(text) / sizeof(char);
    int textWidth = Screen::screen->getFontXsize() * textLength;

    backButton = Button();
    backButton.setBackgroundColor(80, 80, 80);
    backButton.setBorderColor(150, 150, 150);
    backButton.setPosition(1, 1);
    backButton.setDimensions(textWidth + 2, Screen::screen->getFontYsize() + 4);

    backButton.setFont(SmallFont);
    backButton.setTextColor(200, 200, 200);
    backButton.setText(text, textLength);
}

void ColorScreen::initializeColorButtons() {
    int buttonWidth = 50;
    int buttonHeight = 50;

    int colors[9][3] = {
            {255, 255, 255},        // White
            {255, 0,   0},          // Red
            {255, 170, 0},          // Orange
            {255, 255, 0},          // Yellow
            {0,   255, 0},          // Green
            {0,   255, 255},        // Cyan
            {0,   0,   255},        // Blue
            {170, 0,   255},        // Violet
            {255, 0,   255},        // Magenta
    };

    for (int i = 0; i < sizeof(colorButtons) / sizeof(ColorButton); i++) {
        colorButtons[i].setBackgroundColor(colors[i][0], colors[i][1], colors[i][2]);
        colorButtons[i].setDimensions(buttonWidth, buttonHeight);
    }
}

void ColorScreen::display() {
    Screen::screen->clrScr();
    displayBackButton();
    displayColorButtons();
    displayBrightnessSlider();
}

void ColorScreen::displayBackButton() {
    backButton.display(Screen::screen);
}

void ColorScreen::displayColorButtons() {
    int currentX = 10;
    int currentY = 30;
    int buttonMargins = 4;

    for (int i = 0; i < sizeof(colorButtons) / sizeof(ColorButton); i++) {
        colorButtons[i].setPosition(currentX, currentY);
        colorButtons[i].display(Screen::screen);
        getNextButtonPosition(&currentX, &currentY, colorButtons[i], buttonMargins);
    }
}

void ColorScreen::getNextButtonPosition(int *currentX, int *currentY, ColorButton colorButton, int buttonMargins) {
    *currentY = *currentY + buttonMargins + colorButton.height;
    if (*currentY > Screen::screen->getDisplayYSize() - colorButton.height) {
        *currentY = 30;
        *currentX = *currentX + buttonMargins + colorButton.width;
    }
}

void ColorScreen::displayBrightnessSlider() {
    Screen::screen->setColor(150, 150, 150);
    Screen::screen->drawRoundRect(brightnessSliderPositionX,
                                  brightnessSliderPositionY,
                                  brightnessSliderPositionX + brightnessSliderWidth,
                                  brightnessSliderPositionY + brightnessSliderHeight);
    Screen::screen->setFont(SmallFont);
    char text[] = "brightness";
    uint8_t textLength = sizeof(text) / sizeof(char) - 1;
    Screen::screen->print(text,
                          brightnessSliderPositionX + Screen::screen->getFontYsize() + 4,
                          brightnessSliderPositionY + (brightnessSliderHeight - textLength * Screen::screen->getFontXsize()) / 2,
                          90);
}

void ColorScreen::displayBrightnessSliderValue(uint8_t brightness) {
    char text[5];
    sprintf(text, "%3d%%", brightness * 100 / 255);
    Screen::screen->setColor(200, 200, 200);
    Screen::screen->setBackColor(0, 0, 0);
    Screen::screen->setFont(SmallFont);
    // width - textLength * size, to align in center above the slider
    Screen::screen->print(text,
                          brightnessSliderPositionX + (brightnessSliderWidth - 4 * Screen::screen->getFontXsize()) / 2,
                          brightnessSliderPositionY - Screen::screen->getFontYsize() - 2);
}

uint8_t ColorScreen::isBackButtonTouched(uint16_t x, uint16_t y) {
    return backButton.isInBounds(x, y);
}

uint8_t ColorScreen::isColorTouched(uint16_t x, uint16_t y, uint8_t *resultColor) {
    for (int i = 0; i < sizeof(colorButtons) / sizeof(ColorButton); i++) {
        if (colorButtons[i].isInBounds(x, y)) {
            resultColor[0] = colorButtons[i].backgroundColorR;
            resultColor[1] = colorButtons[i].backgroundColorG;
            resultColor[2] = colorButtons[i].backgroundColorB;
            return 1;
        }
    }
    return 0;
}

uint8_t ColorScreen::isBrightnessSliderTouched(uint16_t x, uint16_t y, uint8_t *resultBrightness) {
    uint8_t touchOnSlider = x >= brightnessSliderPositionX && x <= brightnessSliderPositionX + brightnessSliderWidth
                            && y >= brightnessSliderPositionY &&
                            y <= brightnessSliderPositionY + brightnessSliderHeight;
    if (!touchOnSlider) {
        return 0;
    }

    *resultBrightness = 255 - 255 * (y - brightnessSliderPositionY) / brightnessSliderHeight;
    return 1;
}

void ColorScreen::displaySend(uint8_t r, uint8_t g, uint8_t b) {
    char text[25];
    sprintf(text, "Color [%u,%u,%u] send", r, g, b);

    Screen::screen->setFont(SmallFont);
    Screen::screen->setColor(0, 0, 0);
    Screen::screen->fillRect(70, 3, 70 + Screen::screen->getFontXsize() * sizeof(text) / sizeof(char),
                             3 + Screen::screen->getFontYsize()); // Clear line
    Screen::screen->setColor(200, 200, 200);
    Screen::screen->print(text, 70, 3);
}

void ColorScreen::sendColor(uint8_t r, uint8_t g, uint8_t b) {
    // Send info to te other side
    Serial1.write(MH_CMD_COLOR_R);
    Serial1.write(r);
    Serial1.write(MH_CMD_COLOR_G);
    Serial1.write(g);
    Serial1.write(MH_CMD_COLOR_B);
    Serial1.write(b);

    displaySend(r, g, b);
}

void ColorScreen::sendBrightness(uint8_t brightness) {
    Serial1.write(MH_CMD_BRIGHTNESS);
    Serial1.write(brightness);

    displayBrightnessSliderValue(brightness);
}
