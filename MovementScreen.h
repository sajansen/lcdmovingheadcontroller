//
// Created by S. Jansen on 8/12/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_MOVEMENTSCREEN_H
#define LCDMOVINGHEADCONTROLLER_MOVEMENTSCREEN_H

#include "Screen.h"
#include "Button.h"
#include "lib/movingheadcommunicationprotocol/MovingHeadCommunicationProtocol.h"

class MovementScreen : public Screen{
private:
    Button backButton;
    Button recordStartButton;
    Button recordPlayButton;
    uint16_t graphPositionX = 10;
    uint16_t graphPositionY = 30;
    uint16_t graphWidth = 200;
    uint16_t graphHeight = 200;
    uint8_t positionUpperBound = 180;

    uint8_t isRecording = 0;
    uint16_t recordingStep = 0;
    uint16_t recordingLength = 0;
    uint8_t recordingData[1024][2];
    uint8_t isRecordingPlaying = 0;
    uint16_t recordPlayStep = 0;

    void initializeBackButton();
    void initializeRecordStartButton();
    void initializeRecordPlayButton();
    void displayBackButton();
    void displayRecordStartButton();
    void displayRecordPlayButton();
    void displayRecordStatus();
    void displayMovementGraph();
    void displayPositionTextSend(uint8_t x, uint8_t y);
    void recordNewPosition(uint8_t x, uint8_t y);
    void paintPosition(uint8_t x, uint8_t y);
    void handleNextRecordPlayStep();

public:
    MovementScreen(UTFT *screen);
    void display();
    void loop();
    uint8_t isBackButtonTouched(uint16_t x, uint16_t y);
    uint8_t isRecordStartButtonTouched(uint16_t x, uint16_t y);
    uint8_t isRecordPlayButtonTouched(uint16_t x, uint16_t y);
    uint8_t isPositionSelected(uint16_t x, uint16_t y, uint8_t *selectedPosition);
    void sendNewPosition(uint8_t x, uint8_t y);
    void toggleRecording();
    void toggleRecordingPlayback();
};


#endif //LCDMOVINGHEADCONTROLLER_MOVEMENTSCREEN_H
