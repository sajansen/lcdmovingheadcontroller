//
// Created by S. Jansen on 8/12/19.
//

#include "Button.h"

void Button::setText(char *text, int text_length) {
    showOptions |= BUTTON_SHOW_TEXT;
    this->text = (char *) malloc(text_length);
    strcpy(this->text, text);
}

void Button::setBackgroundColor(uint8_t r, uint8_t g, uint8_t b) {
    showOptions |= BUTTON_SHOW_BACKGROUND;
    backgroundColorR = r;
    backgroundColorG = g;
    backgroundColorB = b;
}

void Button::setBorderColor(uint8_t r, uint8_t g, uint8_t b) {
    showOptions |= BUTTON_SHOW_BORDER;
    borderColorR = r;
    borderColorG = g;
    borderColorB = b;
}

void Button::setTextColor(uint8_t r, uint8_t g, uint8_t b) {
    textColorR = r;
    textColorG = g;
    textColorB = b;
}

void Button::setDimensions(int width, int height) {
    this->width = width;
    this->height = height;
}

void Button::display(UTFT *screen) {
    if (showOptions & BUTTON_SHOW_BACKGROUND) {
        screen->setColor(backgroundColorR, backgroundColorG, backgroundColorB);
        screen->fillRoundRect(positionX, positionY, positionX + width,
                              positionY + height);
    }

    if (showOptions & BUTTON_SHOW_BORDER) {
        screen->setColor(borderColorR, borderColorG, borderColorB);
        screen->drawRoundRect(positionX, positionY, positionX + width,
                              positionY + height);
    }

    if (showOptions & BUTTON_SHOW_TEXT) {
        screen->setBackColor(VGA_TRANSPARENT);
        screen->setFont(font);
        screen->setColor(textColorR, textColorG, textColorB);
        screen->print(text, positionX + 4, positionY + (height - screen->getFontYsize()) / 2);
    }
}

void Button::setPosition(int x, int y) {
    positionX = x;
    positionY = y;
}

uint8_t Button::isInBounds(int x, int y) {
    return x > positionX && x < positionX + width
           && y > positionY && y < positionY + height;
}

void Button::setFont(uint8_t *font) {
    this->font = font;
}
