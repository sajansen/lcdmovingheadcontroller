//
// Created by S. Jansen on 8/12/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_BUTTON_H
#define LCDMOVINGHEADCONTROLLER_BUTTON_H

#include <Arduino.h>
#include <UTFT.h>

#define BUTTON_SHOW_BACKGROUND  1
#define BUTTON_SHOW_BORDER      2
#define BUTTON_SHOW_TEXT        4

extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

class Button {
protected:
    uint8_t showOptions = 0;
    char *text;

public:
    Button()= default;;
    void setText(char *text, int text_length);
    void setFont(uint8_t *font);
    void setBackgroundColor(uint8_t r, uint8_t g, uint8_t b);
    void setBorderColor(uint8_t r, uint8_t g, uint8_t b);
    void setTextColor(uint8_t r, uint8_t g, uint8_t b);
    void setDimensions(int width, int height);
    void setPosition(int x, int y);
    void display(UTFT *screen);
    uint8_t isInBounds(int x, int y);

    uint8_t *font;
    int positionX = 0;
    int positionY = 0;
    int width = 0;
    int height = 0;
    uint8_t backgroundColorR = 0;
    uint8_t backgroundColorG = 0;
    uint8_t backgroundColorB = 0;
    uint8_t borderColorR = 0;
    uint8_t borderColorG = 0;
    uint8_t borderColorB = 0;
    uint8_t textColorR = 0;
    uint8_t textColorG = 0;
    uint8_t textColorB = 0;
};


#endif //LCDMOVINGHEADCONTROLLER_BUTTON_H
