//
// Created by S. Jansen on 8/12/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_SCREEN_H
#define LCDMOVINGHEADCONTROLLER_SCREEN_H

#include <Arduino.h>
#include <UTFT.h>

extern uint8_t SmallFont[];
extern uint8_t BigFont[];
extern uint8_t SevenSegNumFont[];

class Screen {
public:
    UTFT *screen;
    Screen(UTFT *screen){
        this->screen = screen;
    };

    virtual void display() {}
    virtual void loop() {}
};


#endif //LCDMOVINGHEADCONTROLLER_SCREEN_H
