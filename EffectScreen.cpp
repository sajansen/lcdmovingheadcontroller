//
// Created by S. Jansen on 8/14/19.
//

#include "EffectScreen.h"

EffectScreen::EffectScreen(UTFT *screen) : Screen(screen) {
    strcpy(effectNames[0], "OFF");
    strcpy(effectNames[1], "SOLID");
    strcpy(effectNames[2], "STROBE");
    strcpy(effectNames[3], "STARS");
    strcpy(effectNames[4], "SMILEY");
    strcpy(effectNames[5], "RAINBOW");

    initializeBackButton();
    initializeEffectButtons();
}

void EffectScreen::initializeBackButton() {
    Screen::screen->setFont(SmallFont);

    char text[] = "Back";
    int textLength = sizeof(text) / sizeof(char);
    int textWidth = Screen::screen->getFontXsize() * textLength;

    backButton = Button();
    backButton.setBackgroundColor(80, 80, 80);
    backButton.setBorderColor(150, 150, 150);
    backButton.setPosition(1, 1);
    backButton.setDimensions(textWidth + 2, Screen::screen->getFontYsize() + 4);

    backButton.setFont(SmallFont);
    backButton.setTextColor(200, 200, 200);
    backButton.setText(text, textLength);
}

void EffectScreen::initializeEffectButtons() {
    int buttonWidth = 80;
    int buttonHeight = 40;

    for (int i = 0; i < sizeof(effectButtons) / sizeof(Button); i++) {
        effectButtons[i].setBackgroundColor(50, 50, 50);
        effectButtons[i].setBorderColor(200, 200, 200);
        effectButtons[i].setTextColor(200, 200, 200);
        effectButtons[i].setDimensions(buttonWidth, buttonHeight);
        effectButtons[i].setFont(SmallFont);
        effectButtons[i].setText(effectNames[i], sizeof(effectNames[0]) / sizeof(char));
    }
}

void EffectScreen::display() {
    Screen::screen->clrScr();
    displayBackButton();
    displayEffectButtons();
    displaySpeedSlider();
}

void EffectScreen::displayBackButton() {
    backButton.display(Screen::screen);
}

void EffectScreen::displayEffectButtons() {
    int currentX = 10;
    int currentY = 30;
    int buttonMargins = 4;

    for (int i = 0; i < sizeof(effectButtons) / sizeof(Button); i++) {
        effectButtons[i].setPosition(currentX, currentY);
        effectButtons[i].display(Screen::screen);
        getNextButtonPosition(&currentX, &currentY, effectButtons[i], buttonMargins);
    }
}

void EffectScreen::getNextButtonPosition(int *currentX, int *currentY, Button effectButton, int buttonMargins) {
    *currentY = *currentY + buttonMargins + effectButton.height;
    if (*currentY > Screen::screen->getDisplayYSize() - effectButton.height) {
        *currentY = 30;
        *currentX = *currentX + buttonMargins + effectButton.width;
    }
}

void EffectScreen::displaySpeedSlider() {
    Screen::screen->setColor(150, 150, 150);
    Screen::screen->drawRoundRect(speedSliderPositionX,
                                  speedSliderPositionY,
                                  speedSliderPositionX + speedSliderWidth,
                                  speedSliderPositionY + speedSliderHeight);
    Screen::screen->setFont(SmallFont);
    char text[] = "speed";
    uint8_t textLength = sizeof(text) / sizeof(char) - 1;
    Screen::screen->print(text,
                          speedSliderPositionX + Screen::screen->getFontYsize() + 4,
                          speedSliderPositionY + (speedSliderHeight - textLength * Screen::screen->getFontXsize()) / 2,
                          90);
}

void EffectScreen::displaySpeedSliderValue(uint8_t speed) {
    char text[5];
    sprintf(text, "%4d", speed);
    Screen::screen->setColor(200, 200, 200);
    Screen::screen->setBackColor(0, 0, 0);
    Screen::screen->setFont(SmallFont);
    // width - textLength * size, to align in center above the slider
    Screen::screen->print(text,
                          speedSliderPositionX + (speedSliderWidth - 4 * Screen::screen->getFontXsize()) / 2,
                          speedSliderPositionY - Screen::screen->getFontYsize() - 2);
}

uint8_t EffectScreen::isBackButtonTouched(uint16_t x, uint16_t y) {
    return backButton.isInBounds(x, y);
}

uint8_t EffectScreen::isEffectButtonTouched(uint16_t x, uint16_t y, uint8_t *resultEffect) {
    for (int i = 0; i < sizeof(effectButtons) / sizeof(Button); i++) {
        if (effectButtons[i].isInBounds(x, y)) {
            *resultEffect = i;
            return 1;
        }
    }
    return 0;
}

uint8_t EffectScreen::isSpeedSliderTouched(uint16_t x, uint16_t y, uint8_t *resultSpeed) {
    uint8_t touchOnSlider = x >= speedSliderPositionX && x <= speedSliderPositionX + speedSliderWidth
                            && y >= speedSliderPositionY &&
                            y <= speedSliderPositionY + speedSliderHeight;
    if (!touchOnSlider) {
        return 0;
    }

    *resultSpeed = 255 - 255 * (y - speedSliderPositionY) / speedSliderHeight;
    return 1;
}

void EffectScreen::displayEffectSend(uint8_t effect) {
    char text[25];
    sprintf(text, "Effect [%s] send", effectNames[effect]);

    Screen::screen->setFont(SmallFont);
    Screen::screen->setColor(0, 0, 0);
    Screen::screen->fillRect(70, 3, 70 + Screen::screen->getFontXsize() * sizeof(text) / sizeof(char),
                             3 + Screen::screen->getFontYsize()); // Clear line
    Screen::screen->setColor(200, 200, 200);
    Screen::screen->print(text, 70, 3);
}

void EffectScreen::sendEffect(uint8_t effect) {
    // Send info to te other side
    Serial1.write(MH_CMD_EFFECT);
    Serial1.write(effect);

    displayEffectSend(effect);
}

void EffectScreen::sendSpeed(uint8_t speed) {
    Serial1.write(MH_CMD_EFFECT_SPEED);
    Serial1.write(speed);

    displaySpeedSliderValue(speed);
}
