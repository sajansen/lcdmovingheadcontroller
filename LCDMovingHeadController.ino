#include <Arduino.h>
#include <UTFT.h>
#include <URTouch.h>
#include "ColorScreen.h"
#include "MenuScreen.h"
#include "MovementScreen.h"
#include "EffectScreen.h"

#define TOUCH_COOLDOWN 1500     // The amount of time in ms that the TouchIcon will be displayed to indicate that a touch event was registered

UTFT screen(SSD1289, 38, 39, 40, 41);
URTouch touch(6, 5, 4, 3, 2);;

Screen *currentScreen;
Screen *newScreen;
MenuScreen menuScreen(&screen);
ColorScreen colorScreen(&screen);
MovementScreen movementScreen(&screen);
EffectScreen effectScreen(&screen);

void displayTouchIcon() {
    screen.setColor(0, 255, 0);
    screen.fillCircle(screen.getDisplayXSize() - 5, 5, 4);
}

void removeTouchIcon() {
    screen.setColor(0, 0, 0);
    screen.fillCircle(screen.getDisplayXSize() - 5, 5, 4);
}

void handleTouch() {
    static unsigned long lastTouchTime;

    if (lastTouchTime + TOUCH_COOLDOWN < millis()) {
        // Clear touch icon
        removeTouchIcon();
    }

    if (!touch.dataAvailable()) {
        return;
    }
    touch.read();
    int16_t touchX = touch.getX();
    int16_t touchY = touch.getY();

    lastTouchTime = millis();
    displayTouchIcon();

    if (currentScreen == &menuScreen) {
        if (menuScreen.isColorScreenButtonTouched(touchX, touchY)) {
            newScreen = &colorScreen;
        } else if (menuScreen.isMovementScreenButtonTouched(touchX, touchY)) {
            newScreen = &movementScreen;
        } else if (menuScreen.isEffectScreenButtonTouched(touchX, touchY)) {
            newScreen = &effectScreen;
        } else if (menuScreen.isDemoScreenButtonTouched(touchX, touchY)) {
            menuScreen.toggleDemo();
            delay(500); // debounce
        }
    } else if (currentScreen == &colorScreen) {
        if (colorScreen.isBackButtonTouched(touchX, touchY)) {
            newScreen = &menuScreen;
            return;
        }

        uint8_t touchedColor[3];
        if (colorScreen.isColorTouched(touchX, touchY, touchedColor)) {
            colorScreen.sendColor(touchedColor[0], touchedColor[1], touchedColor[2]);
            delay(200);
            return;
        }

        uint8_t resultBrightness;
        if (colorScreen.isBrightnessSliderTouched(touchX, touchY, &resultBrightness)) {
            colorScreen.sendBrightness(resultBrightness);
            return;
        }

    } else if (currentScreen == &movementScreen) {
        if (movementScreen.isBackButtonTouched(touchX, touchY)) {
            newScreen = &menuScreen;
            return;
        }

        uint8_t selectedPosition[2];
        if (movementScreen.isPositionSelected(touchX, touchY, selectedPosition)) {
            movementScreen.sendNewPosition(selectedPosition[0], selectedPosition[1]);
            return;
        }

        if (movementScreen.isRecordStartButtonTouched(touchX, touchY)) {
            movementScreen.toggleRecording();
            delay(500);
            return;
        }

        if (movementScreen.isRecordPlayButtonTouched(touchX, touchY)) {
            movementScreen.toggleRecordingPlayback();
            delay(500);
            return;
        }
    } else if (currentScreen == &effectScreen) {
        if (effectScreen.isBackButtonTouched(touchX, touchY)) {
            newScreen = &menuScreen;
            return;
        }

        uint8_t touchedEffect;
        if (effectScreen.isEffectButtonTouched(touchX, touchY, &touchedEffect)) {
            effectScreen.sendEffect(touchedEffect);
            delay(200);
            return;
        }

        uint8_t resultSpeed;
        if (effectScreen.isSpeedSliderTouched(touchX, touchY, &resultSpeed)) {
            effectScreen.sendSpeed(resultSpeed);
            return;
        }

    }
}

void handleScreenState() {
    if (currentScreen != newScreen) {
        currentScreen = newScreen;
        currentScreen->display();
    }
}

void setup() {
    pinMode(LED_BUILTIN, OUTPUT);

    screen.InitLCD(LANDSCAPE);
    screen.setBackColor(VGA_TRANSPARENT);

    touch.InitTouch();
    touch.setPrecision(PREC_HI);

    newScreen = &menuScreen;

    Serial.begin(MH_BAUD_RATE);
    Serial1.begin(MH_BAUD_RATE);
    delay(100);
    Serial.print(F("Initialized v0.0.1... "));
}

void loop() {
    handleScreenState();
    handleTouch();

    currentScreen->loop();
}