//
// Created by S. Jansen on 8/10/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_COLORSCREEN_H
#define LCDMOVINGHEADCONTROLLER_COLORSCREEN_H

#include "Screen.h"
#include "ColorButton.h"
#include "Button.h"
#include "lib/movingheadcommunicationprotocol/MovingHeadCommunicationProtocol.h"

class ColorScreen: public Screen {
private:
    ColorButton colorButtons[9];
    Button backButton;
    uint16_t brightnessSliderPositionX = 270;
    uint16_t brightnessSliderPositionY = 40;
    uint16_t brightnessSliderWidth = 20;
    uint16_t brightnessSliderHeight = 150;

    void initializeBackButton();
    void initializeColorButtons();
    void displayBackButton();
    void displayColorButtons();
    void displayBrightnessSlider();
    void displaySend(uint8_t r, uint8_t g, uint8_t b);
    void displayBrightnessSliderValue(uint8_t brightness);
    void getNextButtonPosition(int *currentX, int *currentY,  ColorButton colorButton, int buttonMargins);

public:
    ColorScreen(UTFT *screen);
    void display();
    uint8_t isColorTouched(uint16_t x, uint16_t y, uint8_t *resultColor);
    uint8_t isBackButtonTouched(uint16_t x, uint16_t y);
    uint8_t isBrightnessSliderTouched(uint16_t x, uint16_t y, uint8_t *resultBrightness);
    void sendColor(uint8_t r, uint8_t g, uint8_t b);
    void sendBrightness(uint8_t brightness);
};


#endif //LCDMOVINGHEADCONTROLLER_COLORSCREEN_H
