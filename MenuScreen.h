//
// Created by S. Jansen on 8/12/19.
//

#ifndef LCDMOVINGHEADCONTROLLER_MENUSCREEN_H
#define LCDMOVINGHEADCONTROLLER_MENUSCREEN_H

#include "Screen.h"
#include "Button.h"
#include "lib/movingheadcommunicationprotocol/MovingHeadCommunicationProtocol.h"

class MenuScreen: public Screen {
private:
    Button colorScreenButton;
    Button movementScreenButton;
    Button effectScreenButton;
    Button demoScreenButton;

    uint8_t isDemoOn = 0;

    void initializeScreenButtons();
    void displayScreenTitle();

public:
    MenuScreen(UTFT *screen);
    void display();
    uint8_t isColorScreenButtonTouched(uint16_t x, uint16_t y);
    uint8_t isMovementScreenButtonTouched(uint16_t x, uint16_t y);
    uint8_t isEffectScreenButtonTouched(uint16_t x, uint16_t y);
    uint8_t isDemoScreenButtonTouched(uint16_t x, uint16_t y);

    void toggleDemo();
};


#endif //LCDMOVINGHEADCONTROLLER_MENUSCREEN_H
