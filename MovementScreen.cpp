//
// Created by S. Jansen on 8/12/19.
//

#include "MovementScreen.h"

MovementScreen::MovementScreen(UTFT *screen) : Screen(screen) {
    initializeBackButton();
    initializeRecordStartButton();
    initializeRecordPlayButton();
}

void MovementScreen::initializeBackButton() {
    Screen::screen->setFont(SmallFont);

    char text[] = "Back";
    int textLength = sizeof(text) / sizeof(char);
    int textWidth = Screen::screen->getFontXsize() * textLength;

    backButton = Button();
    backButton.setBackgroundColor(80, 80, 80);
    backButton.setBorderColor(150, 150, 150);
    backButton.setPosition(1, 1);
    backButton.setDimensions(textWidth + 2, Screen::screen->getFontYsize() + 4);

    backButton.setFont(SmallFont);
    backButton.setTextColor(200, 200, 200);
    backButton.setText(text, textLength);
}

void MovementScreen::initializeRecordStartButton() {
    recordStartButton = Button();
    recordStartButton.setBackgroundColor(130, 130, 130);
    recordStartButton.setBorderColor(180, 180, 180);
    recordStartButton.setPosition(230, 30);
    recordStartButton.setDimensions(40, 40);
}

void MovementScreen::initializeRecordPlayButton() {
    recordPlayButton = Button();
    recordPlayButton.setBackgroundColor(130, 130, 130);
    recordPlayButton.setBorderColor(180, 180, 180);
    recordPlayButton.setPosition(230, 80);
    recordPlayButton.setDimensions(40, 40);

    recordPlayButton.setFont(BigFont);
    recordPlayButton.setTextColor(20, 20, 20);
}

void MovementScreen::display() {
    Screen::screen->clrScr();
    displayBackButton();
    displayRecordStartButton();
    displayRecordPlayButton();
    displayMovementGraph();
}

void MovementScreen::displayBackButton() {
    backButton.display(Screen::screen);
}

void MovementScreen::displayRecordStartButton() {
    recordStartButton.display(Screen::screen);

    displayRecordStatus();
}

void MovementScreen::displayRecordPlayButton() {
    if (isRecordingPlaying) {
        recordPlayButton.setText("||", 2);
    } else {
        recordPlayButton.setText(">", 1);
    }
    recordPlayButton.display(Screen::screen);
}

void MovementScreen::displayMovementGraph() {
    Screen::screen->setColor(150, 150, 150);
    Screen::screen->drawRect(graphPositionX,
                             graphPositionY,
                             graphPositionX + graphWidth,
                             graphPositionY + graphHeight);
    Screen::screen->drawHLine(graphPositionX - 7,
                              graphPositionY + graphHeight / 2,
                              graphWidth + 14);
    Screen::screen->drawVLine(graphPositionX + graphWidth / 2,
                              graphPositionY - 7,
                              graphHeight + 14);
}

uint8_t MovementScreen::isBackButtonTouched(uint16_t x, uint16_t y) {
    return backButton.isInBounds(x, y);
}

uint8_t MovementScreen::isRecordStartButtonTouched(uint16_t x, uint16_t y) {
    return recordStartButton.isInBounds(x, y);
}

uint8_t MovementScreen::isRecordPlayButtonTouched(uint16_t x, uint16_t y) {
    return recordPlayButton.isInBounds(x, y);
}

void MovementScreen::displayPositionTextSend(uint8_t x, uint8_t y) {
    char text[23];
    sprintf(text, "Position [%u;%u] send", x, y);

    Screen::screen->setFont(SmallFont);
    Screen::screen->setColor(0, 0, 0);
    Screen::screen->fillRect(70, 3, 70 + Screen::screen->getFontXsize() * sizeof(text) / sizeof(char),
                             3 + Screen::screen->getFontYsize()); // Clear line
    Screen::screen->setColor(200, 200, 200);
    Screen::screen->print(text, 70, 3);
}

void MovementScreen::sendNewPosition(uint8_t x, uint8_t y) {
    // Send info to te other side
    Serial1.write(MH_CMD_POSITION_X);
    Serial1.write(x);
    Serial1.write(MH_CMD_POSITION_Y);
    Serial1.write(y);

    if (isRecording) {
        recordNewPosition(x, y);
        return;
    }
    if (isRecordingPlaying) {
        return;
    }

//    displayPositionTextSend(x, y);
    delay(5);
}

uint8_t MovementScreen::isPositionSelected(uint16_t x, uint16_t y, uint8_t *selectedPosition) {
    uint8_t touchOnGraph = x >= graphPositionX && x <= graphPositionX + graphWidth
                           && y >= graphPositionY && y <= graphPositionY + graphHeight;
    if (!touchOnGraph) {
        return 0;
    }

    selectedPosition[0] = positionUpperBound * (x - graphPositionX) / graphWidth;
    selectedPosition[1] = positionUpperBound * (y - graphPositionY) / graphHeight;
    return 1;
}

void MovementScreen::displayRecordStatus() {
    if (isRecording) {
        Screen::screen->setColor(255, 0, 0);
    } else {
        Screen::screen->setColor(100, 20, 20);
    }

    Screen::screen->fillCircle(recordStartButton.positionX + recordStartButton.width / 2,
                               recordStartButton.positionY + recordStartButton.height / 2,
                               5);
}

void MovementScreen::toggleRecording() {
    isRecording = !isRecording;
    displayRecordStatus();

    if (isRecording) {
        recordingStep = 0;

        if (isRecordingPlaying) {
            toggleRecordingPlayback();
        }
        return;
    }

    recordingLength = recordingStep + 1;

    // Clear graph from the position dots we've painted during recording
    Screen::screen->setColor(0, 0, 0);
    Screen::screen->fillRect(graphPositionX,
                             graphPositionY,
                             graphPositionX + graphWidth,
                             graphPositionY + graphHeight);
    displayMovementGraph();
}

void MovementScreen::recordNewPosition(uint8_t x, uint8_t y) {
    recordingData[recordingStep][0] = x;
    recordingData[recordingStep][1] = y;
    recordingStep = ++recordingStep % 1024;

    paintPosition(x, y);
}

void MovementScreen::paintPosition(uint8_t x, uint8_t y) {
    Screen::screen->setColor(180, 180, 255);
    Screen::screen->drawPixel(graphPositionX + graphWidth * x / positionUpperBound,
                              graphPositionY + graphHeight * y / positionUpperBound);
}

void MovementScreen::toggleRecordingPlayback() {
    isRecordingPlaying = !isRecordingPlaying;
    recordPlayStep = 0;
    displayRecordPlayButton();

    if (isRecordingPlaying && isRecording) {
        toggleRecording();
    }
}

void MovementScreen::loop() {
//    Screen::loop();

    if (isRecordingPlaying) {
        handleNextRecordPlayStep();
        delay(5);
    }
}

void MovementScreen::handleNextRecordPlayStep() {
    sendNewPosition(recordingData[recordPlayStep][0],
                    recordingData[recordPlayStep][1]);

    recordPlayStep = ++recordPlayStep % recordingLength;
}
