//
// Created by S. Jansen on 8/12/19.
//

#include "MenuScreen.h"

MenuScreen::MenuScreen(UTFT *screen) : Screen(screen) {
    initializeScreenButtons();
}

void MenuScreen::initializeScreenButtons() {
    uint16_t currentPositionY = 40;

    colorScreenButton = Button();
    char colorScreenButtonText[] = "Color";
    colorScreenButton.setFont(BigFont);
    colorScreenButton.setText(colorScreenButtonText, sizeof(colorScreenButtonText) / sizeof(char));

    colorScreenButton.setPosition(40, currentPositionY);
    currentPositionY += 40;
    colorScreenButton.setDimensions(200, 30);

    colorScreenButton.setBackgroundColor(255, 170, 100);
    colorScreenButton.setBorderColor(200, 150, 100);
    colorScreenButton.setTextColor(100, 66, 33);
    
    movementScreenButton = Button();
    char movementScreenButtonText[] = "Movement";
    movementScreenButton.setFont(BigFont);
    movementScreenButton.setText(movementScreenButtonText, sizeof(movementScreenButtonText) / sizeof(char));

    movementScreenButton.setPosition(40, currentPositionY);
    currentPositionY += 40;
    movementScreenButton.setDimensions(200, 30);

    movementScreenButton.setBackgroundColor(255, 170, 100);
    movementScreenButton.setBorderColor(200, 150, 100);
    movementScreenButton.setTextColor(100, 66, 33);
    
    effectScreenButton = Button();
    char effectScreenButtonText[] = "Effect";
    effectScreenButton.setFont(BigFont);
    effectScreenButton.setText(effectScreenButtonText, sizeof(effectScreenButtonText) / sizeof(char));

    effectScreenButton.setPosition(40, currentPositionY);
    currentPositionY += 40;
    effectScreenButton.setDimensions(200, 30);

    effectScreenButton.setBackgroundColor(255, 170, 100);
    effectScreenButton.setBorderColor(200, 150, 100);
    effectScreenButton.setTextColor(100, 66, 33);

    demoScreenButton = Button();
    char demoScreenButtonText[] = "Demo";
    demoScreenButton.setFont(BigFont);
    demoScreenButton.setText(demoScreenButtonText, sizeof(demoScreenButtonText) / sizeof(char));

    demoScreenButton.setPosition(40, currentPositionY);
    currentPositionY += 40;
    demoScreenButton.setDimensions(200, 30);

    demoScreenButton.setBackgroundColor(255, 140, 80);
    demoScreenButton.setBorderColor(200, 110, 80);
    demoScreenButton.setTextColor(100, 66, 33);
}

void MenuScreen::display() {
    Screen::screen->clrScr();
    Screen::screen->setBackColor(VGA_TRANSPARENT);
    displayScreenTitle();
    colorScreenButton.display(Screen::screen);
    movementScreenButton.display(Screen::screen);
    effectScreenButton.display(Screen::screen);
    demoScreenButton.display(Screen::screen);
}

void MenuScreen::displayScreenTitle() {
    Screen::screen->setFont(BigFont);
    Screen::screen->setColor(255, 170, 100);
    Screen::screen->print("Menu", CENTER, 1);

    Screen::screen->setColor(200, 150, 100);
    Screen::screen->drawHLine(0, 1 + Screen::screen->getFontYsize() + 4, Screen::screen->getDisplayXSize() - 1);
}

uint8_t MenuScreen::isColorScreenButtonTouched(uint16_t x, uint16_t y) {
    return colorScreenButton.isInBounds(x, y);
}

uint8_t MenuScreen::isMovementScreenButtonTouched(uint16_t x, uint16_t y) {
    return movementScreenButton.isInBounds(x, y);
}

uint8_t MenuScreen::isEffectScreenButtonTouched(uint16_t x, uint16_t y) {
    return effectScreenButton.isInBounds(x, y);
}

uint8_t MenuScreen::isDemoScreenButtonTouched(uint16_t x, uint16_t y) {
    return demoScreenButton.isInBounds(x, y);
}

void MenuScreen::toggleDemo() {
    isDemoOn = !isDemoOn;
    Serial1.write(MH_CMD_DEMO);
    Serial1.write(isDemoOn);
}

